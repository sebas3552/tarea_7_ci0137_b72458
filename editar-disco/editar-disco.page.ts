import { Component, OnInit } from '@angular/core';
import { Disco } from '../models/Disco';
import { RestService } from '../rest.service';
import { LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-editar-disco',
  templateUrl: './editar-disco.page.html',
  styleUrls: ['./editar-disco.page.scss'],
})
export class EditarDiscoPage implements OnInit {

  disco : Disco
  constructor(private restService: RestService, private loadingCtrl: LoadingController, private navCtrl: NavController) { 
    this.disco = new Disco();
  }
  ngOnInit() {

  }
  async guardar() {
    let loader = await this.loadingCtrl.create({
      spinner: "circles"
    });
    loader.present();
    await this.restService.updateDisco(this.disco.id, this.disco).subscribe((response) => {
      loader.dismiss();
      this.navCtrl.navigateBack('/home');
    });
    
  }
}
