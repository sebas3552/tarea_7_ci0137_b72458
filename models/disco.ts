export class Disco {
    id: number;
    artist: string;
    releaseDate: Date;
    title: string;
}
