

import { Component, OnInit } from '@angular/core';
import { ContactoService } from 'src/backend/ContactoService';
import { LoadingController, NavController } from '@ionic/angular';
import { Disco } from '../models/disco'
import { RestService } from '../rest.service';
@Component({
  selector: 'app-crear-disco',
  templateUrl: './crear-disco.page.html',
  styleUrls: ['./crear-disco.page.scss'],
})
export class CrearDiscoPage implements OnInit {
  disco : Disco
  constructor(private restService: RestService, private loadingCtrl: LoadingController, private navCtrl: NavController) { 
    this.disco = new Disco();
  }
  ngOnInit() {

  }
  async guardar() {
    let loader = await this.loadingCtrl.create({
      spinner: "circles"
    });
    loader.present();
    await this.restService.createDisco(this.disco).subscribe((response) => {
      loader.dismiss();
      this.navCtrl.navigateBack('/home');
    });
    
  }
}
