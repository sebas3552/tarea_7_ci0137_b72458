import { Component } from '@angular/core';
import { ContactoService } from 'src/backend/ContactoService';
import { RestService } from '../rest.service'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  discos : any;
  constructor(private contactoService : ContactoService,
              private restService : RestService
    ) {
  }
  ngOnInit() {
    this.doRefresh();
  }
  async doRefresh() {
    console.log("refrescar");
    this.restService.getDiscos()
    .then(data => {
      this.discos = data;
    });
  }

  delete(id)
  {
    this.restService.deleteDisco(id).subscribe(Response => {
      //Update list after delete is successful
      this.restService.getDiscos();
    });
    console.log(id);
  }
}
